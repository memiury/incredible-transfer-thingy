use crossterm::{
    event::{self, DisableMouseCapture, EnableMouseCapture, Event, KeyCode},
    execute,
    terminal::{disable_raw_mode, enable_raw_mode, EnterAlternateScreen, LeaveAlternateScreen},
};
use std::io;
use tui::{
    backend::{Backend, CrosstermBackend},
    layout::{Constraint, Direction, Layout},
    style::{Color, Modifier, Style},
    text::{Span, Spans},
    widgets::{Block, Borders, Tabs},
    Frame, Terminal,
};

enum InputMode {
    Normal,
    Editing,
}

struct App<'a> {
    titles: Vec<&'a str>,
    index: usize,
    input_mode: InputMode,
    input: String,
    messages: Vec<String>,
}

impl<'a> App<'a> {
    fn new() -> App<'a> {
        App {
            titles: vec!["Chat", "placeholder"],
            index: 0,
            input_mode: InputMode::Normal,
            input: String::from(""),
            messages: vec![],
        }
    }

    pub fn next(&mut self) {
        self.index = (self.index + 1) % self.titles.len();
    }

    pub fn previous(&mut self) {
        if self.index > 0 {
            self.index -= 1;
        } else {
            self.index = self.titles.len() - 1;
        }
    }
}

fn run_app<B: Backend>(terminal: &mut Terminal<B>, mut app: App) -> io::Result<()> {
    loop {
        terminal.draw(|f| ui(f, &app))?;

        if let Event::Key(key) = event::read()? {
            match app.input_mode {
                InputMode::Normal => match key.code {
                    KeyCode::Char('e') => app.input_mode = InputMode::Editing,
                    KeyCode::Char('q') => return Ok(()),
                    KeyCode::Right => app.next(),
                    KeyCode::Tab => app.next(),
                    KeyCode::Left => app.previous(),
                    _ => {}
                },
                InputMode::Editing => match key.code {
                    KeyCode::Enter => app.messages.push(app.input.drain(..).collect()),
                    KeyCode::Char(c) => app.input.push(c),
                    KeyCode::Backspace => {
                        app.input.pop();
                    }
                    KeyCode::Esc => app.input_mode = InputMode::Normal,
                    _ => {}
                },
            }
        }
    }
}

//TODO stwórz tui::widgets::? do zakładki z Chatem
fn get_chat() -> Block<'static> {
    Block::default()
        .title("Chat")
        .style(Style::default().fg(Color::Green).bg(Color::Magenta))
        .borders(Borders::ALL)
}

//TODO stwórz tui::widgets::? do zakładki z placeholder
fn get_placeholder() -> Block<'static> {
    Block::default()
        .title("whateva")
        .style(Style::default().fg(Color::Red).bg(Color::Cyan))
        .borders(Borders::ALL)
}

fn ui<B: Backend>(f: &mut Frame<B>, app: &App) {
    let size = f.size();
    let chunks = Layout::default()
        .direction(Direction::Vertical)
        .margin(1)
        .constraints([Constraint::Length(3), Constraint::Min(0)].as_ref()) //TODO dodaj trzeci?
        .split(size);

    //TODO uwzględnij input i messages w układzie!

    let block = Block::default();
    f.render_widget(block, size);
    let titles = app
        .titles
        .iter()
        .map(|t| {
            let (first, rest) = t.split_at(1);
            Spans::from(vec![
                Span::styled(first, Style::default().fg(Color::Green)),
                Span::styled(rest, Style::default().fg(Color::Red)),
            ])
        })
        .collect();
    let tabs = Tabs::new(titles)
        .block(Block::default().borders(Borders::ALL).title("Tabs"))
        .select(app.index)
        .style(Style::default())
        .highlight_style(
            Style::default()
                .add_modifier(Modifier::BOLD)
                .bg(Color::Gray),
        );
    f.render_widget(tabs, chunks[0]);
    let inner = match app.index {
        0 => get_chat(),
        1 => get_placeholder(),
        _ => unreachable!(),
    };
    f.render_widget(inner, chunks[1]);
}

fn main() -> anyhow::Result<()> {
    // setup terminal
    enable_raw_mode()?;
    let mut stdout = io::stdout();
    execute!(stdout, EnterAlternateScreen, EnableMouseCapture)?;
    let backend = CrosstermBackend::new(stdout);
    let mut terminal = Terminal::new(backend)?;

    // create app and run it
    let app = App::new();
    let res = run_app(&mut terminal, app);

    // restore terminal
    disable_raw_mode()?;
    execute!(
        terminal.backend_mut(),
        LeaveAlternateScreen,
        DisableMouseCapture
    )?;
    terminal.show_cursor()?;

    if let Err(err) = res {
        println!("{:?}", err)
    }

    Ok(())
}
